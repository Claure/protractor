
exports.config ={
    seleniumAddress: "http://localhost:4444/wd/hub",
    suites:{
        homepage: 'spec.js',
        smoke_specs: ['sumeOneSpec.js',
                    'sumeOneSpec1.js'
                    ]

    },
    capabilities:{
        browserName: "chrome",
        chromeOptions: {args: ['--lang=en', '--window-size=1600,1200']}
    }

};
