describe("Multiple elements", function () {
    var first = element(by.model('first'));
    var second = element(by.model('second'));
    var goButton = element(by.id('gobutton'));
    var result = element(by.className('ng-binding'));
    var history = element.all(by.repeater('result in memory'));

    
    beforeEach(function () {
        browser.get('https://juliemr.github.io/protractor-demo/');

    });
    //TC1
    it("should have title", () => {
        expect(browser.getTitle()).toEqual('Super Calculator');
    });

    //TC2
    it("should add one and two", () => {
        first.sendKeys(1);
        second.sendKeys(3);
        goButton.click();
        expect(result.getText()).toEqual('4');
    });
    //TC3
    it("should 8 ", () => {
        first.sendKeys(5);
        second.sendKeys(3);
        goButton.click();
        expect(result.getText()).toEqual('8');
    });
    it("Should read a value form the input ", () => {
        first.sendKeys(5);
        expect(first.getAttribute('value')).toEqual('5');
    });
    
    function addNumbers(a,b){
    first.sendKeys(5);
    second.sendKeys(6);
    goButton.click();
    }
    it("should checkt the history",()=>{
    addNumbers(4,5);
    addNumbers(5,6);
    addNumbers(32,9);
    expect(history.count()).toEqual(3);
    });
});
