exports.config ={
    seleniumAddress: "http://localhost:4444/wd/hub",
    specs: ["spec.js"],
    framework: 'jasmine2',
    capabilities:{
      browserName: "chrome",
      chromeOptions: {args: ['--lang=en', '--window-size=1600,1200']},
      
  },
  "helpers": [
   "helpers/**/*.js"
 ],
 "oneFailurePerSpec": true,
 "config": {
   "random": false
 },
  onPrepare: function() {
   browser.ignoreSinchronization=true;
   browser.driver.manage().window().maximize();
   var AllureReporter = require('jasmine-allure-reporter');
   jasmine.getEnv().addReporter(new AllureReporter({
     resultsDir: 'allure-results'
   }));

   jasmine.getEnv().addReporter(new AllureReporter());
    jasmine.getEnv().afterEach(function(done){
      browser.takeScreenshot().then(function (png) {
        allure.createAttachment('Screenshot', function () {
          return new Buffer(png, 'base64')
        }, 'image/png')();
        done();
      })
    });
 }

};
